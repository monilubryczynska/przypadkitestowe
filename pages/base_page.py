from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait

from settings import CHROMEDRIVER_PATH


class BasePage:

    def __init__(self, driver=None):
        self.driver = driver if driver is not None else self._driver()
        self.wait = WebDriverWait(driver, timeout=10)
        self.driver.implicitly_wait(15)
        self.driver.maximize_window()

    @staticmethod
    def _driver():
        return webdriver.Chrome(CHROMEDRIVER_PATH)

    def close_driver(self):
        self.driver.close()