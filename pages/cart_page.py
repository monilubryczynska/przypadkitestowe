
import time

from selenium.webdriver import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions, wait
from selenium.webdriver.support.wait import WebDriverWait

from base_page import BasePage
from settings import CART_URL
class PlaceOrder(BasePage):
    CART_NAME = "Monika"
    CART_COUNTRY ="Grecja"
    CART_CITY = "Saloniki"
    CREDIT_CART = 12345667
    CART_MONTH = "lipiec"
    CART_YEAR = 2050
    def open(self):
        self.driver.get(CART_URL)
    def click_button_place_order(self):
        el_button = self.driver.find_element(By.XPATH, "//*[@class='btn btn-success']")
        el_button.click()

    def input_name(self):
        self.wait.until(expected_conditions.presence_of_element_located((By.ID, "name")))
        input_name = self.driver.find_element(By.ID, 'name')
        input_name.send_keys(self.CART_NAME)
    def input_country(self):
        input_country = self.driver.find_element(By.ID,'country')
        input_country.send_keys(self.CART_COUNTRY)
    def input_city(self):
        self.wait.until(expected_conditions.presence_of_element_located((By.ID, 'city')))
        input_city = self.driver.find_element(By.ID,'city')
        input_city.send_keys(self.CART_CITY)
    def input_credit_cart(self):
        input_credit_cart = self.driver.find_element(By.ID,"card")
        input_credit_cart.send_keys(self.CART_CITY)
    def input_month(self):
        input_month = self.driver.find_element(By.ID,'month')
        input_month.send_keys(self.CART_MONTH)
    def input_year(self):
        input_month = self.driver.find_element(By.ID,'year')
        input_month.send_keys(self.CART_YEAR)
    def click_button_purchase(self):
        el_button_purchase = self.driver.find_element(By.XPATH, "//*[@id='orderModal']//*[@class='btn btn-primary']")
        el_button_purchase.click()
    def assert_text(self):

        return self.driver.find_element(By.XPATH,".//h2[contains(text(),'Thank you for your purchase!')]").is_displayed()


