

from selenium.webdriver import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions, wait
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.alert import Alert

from base_page import BasePage
from settings import BASE_URL

class CategoriesPage(BasePage):

    def open(self):
        self.driver.get(BASE_URL)

    def select_laptop_category(self):
        el_laptop_category = self. driver.find_element(By.XPATH, "//*[text()='Laptops']")
        el_laptop_category.send_keys(Keys.ENTER)

    def select_laptop(self):
        self.wait.until(expected_conditions.presence_of_element_located((By.CSS_SELECTOR, "[class='card h-100'] [href='prod.html?idp_=9']")))
        el_laptop = self.driver.find_element(By.CSS_SELECTOR,"[class='card h-100'] [href='prod.html?idp_=9']")
        el_laptop.send_keys(Keys.ENTER)


    def click_to_cart_button(self):
        el_btn = self.driver.find_element(By.XPATH, "//*[text()='Add to cart']")
        el_btn.click()

    def assert_alert_accept(self):
        alert = Alert(self.driver)
        print(alert.text)
        alert.accept()






