
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.alert import Alert

from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions, wait


from base_page import BasePage
from settings import BASE_URL


class IncorrectLoginPage(BasePage):
    STORE_INCORRECT_LOGIN = "MoXXX"
    STORE_INCORRECT_PASSWORD = "sdfghjkl01*"



    def open(self):
        self.driver.get(BASE_URL)


    def open_the_login_window(self):
        self.wait.until(expected_conditions.presence_of_element_located((By.ID, "login2"))).click()

    def login(self):

        self.fill_incorrect_username_and_password()
        self.click_submit()

    def fill_incorrect_username_and_password(self):
        self.input_incorrect_username()
        self.input_incorrect_password()

    def input_incorrect_username(self):
        self.wait.until(expected_conditions.presence_of_element_located((By.ID, "loginusername")))
        input_incorrect_username = self.driver.find_element(By.ID, "loginusername")
        input_incorrect_username.send_keys(self. STORE_INCORRECT_LOGIN)


    def input_incorrect_password(self):
        input_incorrect_password = self.driver.find_element(By.ID,"loginpassword")
        input_incorrect_password.send_keys(self.STORE_INCORRECT_PASSWORD)


    def click_submit(self):
        self.wait.until(expected_conditions.presence_of_element_located((By.ID, "logInModal"))).click()

    def assert_alert_accept(self):
        alert = Alert(self.driver)
        print(alert.text)
        alert.accept()
