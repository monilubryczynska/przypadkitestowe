
from selenium.common.exceptions import TimeoutException

from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions, wait


from base_page import BasePage
from settings import BASE_URL


class LoginPage(BasePage):
    STORE_LOGIN = "MonikaXXX"
    STORE_PASSWORD = "StudiaAutomaty01*"



    def open(self):
        self.driver.get(BASE_URL)


    def open_the_login_window(self):
        self.wait.until(expected_conditions.presence_of_element_located((By.ID, "login2"))).click()

    def login(self):

        self.fill_correct_username_and_password()
        self.click_submit()

    def fill_correct_username_and_password(self):
        self.input_username()
        self.input_password()

    def input_username(self):
        #self.wait.until(expected_conditions.presence_of_element_located((By.CSS_SELECTOR, "//*[@id='logInModalLabel']")))
        self.wait.until(expected_conditions.presence_of_element_located((By.ID, "loginusername")))
        input_username = self.driver.find_element(By.ID, "loginusername")
        input_username.send_keys(self.STORE_LOGIN)


    def input_password(self):
        input_password = self.driver.find_element(By.ID,"loginpassword")
        input_password.send_keys(self.STORE_PASSWORD)


    def click_submit(self):
        self.wait.until(expected_conditions.presence_of_element_located((By.ID, "logInModal"))).click()

    def assert_login_is_succeed(self):
        try:
            self.wait.until(expected_conditions.presence_of_element_located((By.ID, "nameofuser")))
        except TimeoutException:
            raise AssertionError("can not find element with element id = nameofuser")





