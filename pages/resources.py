from _pytest.fixtures import fixture
from selenium import webdriver
from settings import CHROMEDRIVER_PATH
from selenium.webdriver.remote.webdriver import WebDriver

@fixture
def driver() -> WebDriver:
    driver = webdriver.Chrome(CHROMEDRIVER_PATH)
    driver.implicitly_wait(5)
    driver.maximize_window()
    yield driver
    driver.close()


@fixture
def driver_with_logged_user(driver: WebDriver) -> WebDriver:
    from login_page import LoginPage
    login_page = LoginPage(driver)
    login_page.open()
    login_page.open_the_login_window()
    login_page.login()
    return driver
