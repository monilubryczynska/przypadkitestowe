import time

from cart_page import PlaceOrder

from resources import driver





def test_user_can_place_order(driver):
    place_order = PlaceOrder(driver)
    place_order.open()
    place_order.click_button_place_order()
    place_order.input_name()
    place_order.input_country()
    place_order.input_city()
    place_order.input_credit_cart()
    place_order.input_month()
    place_order.input_year()



