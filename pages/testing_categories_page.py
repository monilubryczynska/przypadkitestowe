

from categories_page import CategoriesPage
from _pytest.fixtures import fixture
from selenium import webdriver
from settings import CHROMEDRIVER_PATH

@fixture
def driver():
    driver = webdriver.Chrome(CHROMEDRIVER_PATH)
    yield driver
    driver.close()


def test_user_can_select_laptop(driver):
    categories_page = CategoriesPage(driver)
    categories_page.open()
    categories_page.select_laptop_category()
    categories_page.select_laptop()
    categories_page.click_to_cart_button()

