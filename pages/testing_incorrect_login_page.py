import time

from incorrect_login_page import IncorrectLoginPage

from resources import driver

def test_user_can_login(driver):

    incorrect_loin_page = IncorrectLoginPage(driver)
    incorrect_loin_page.open()
    incorrect_loin_page.open_the_login_window()
    incorrect_loin_page.login()
    time.sleep(3)
    incorrect_loin_page.assert_alert_accept()