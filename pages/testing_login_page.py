import time

from login_page import LoginPage
from _pytest.fixtures import fixture
from selenium import webdriver
from settings import CHROMEDRIVER_PATH

@fixture
def driver():
    driver = webdriver.Chrome(CHROMEDRIVER_PATH)
    yield driver
    driver.close()

def test_user_can_login(driver):

    login_page = LoginPage(driver)
    login_page.open()
    login_page.open_the_login_window()
    login_page.login()
    login_page.assert_login_is_succeed()
