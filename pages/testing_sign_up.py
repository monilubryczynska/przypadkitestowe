import time

from _pytest.fixtures import fixture
from selenium import webdriver
from sign_up import SignUp
from settings import CHROMEDRIVER_PATH


@fixture
def driver():
    driver = webdriver.Chrome(CHROMEDRIVER_PATH)
    yield driver
    driver.close()



def test_user_can_sign_up(driver):
    sign_up = SignUp(driver)
    sign_up.open()
    time.sleep(3)
    sign_up.open_the_sign_up_window()
    time.sleep(3)
    #sign_up.input_sign_up_username()